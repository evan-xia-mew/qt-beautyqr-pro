QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    D:/app/qrcode/qrencode-master/qrencode/bitstream.c \
    D:/app/qrcode/qrencode-master/qrencode/mask.c \
    D:/app/qrcode/qrencode-master/qrencode/mmask.c \
    D:/app/qrcode/qrencode-master/qrencode/mqrspec.c \
    D:/app/qrcode/qrencode-master/qrencode/qrencode.c \
    D:/app/qrcode/qrencode-master/qrencode/qrinput.c \
    D:/app/qrcode/qrencode-master/qrencode/qrspec.c \
    D:/app/qrcode/qrencode-master/qrencode/rscode.c \
    D:/app/qrcode/qrencode-master/qrencode/split.c \
    main.cpp \
    widget.cpp

HEADERS += \
    D:/app/qrcode/qrencode-master/qrencode/bitstream.h \
    D:/app/qrcode/qrencode-master/qrencode/config.h \
    D:/app/qrcode/qrencode-master/qrencode/mask.h \
    D:/app/qrcode/qrencode-master/qrencode/mmask.h \
    D:/app/qrcode/qrencode-master/qrencode/mqrspec.h \
    D:/app/qrcode/qrencode-master/qrencode/qrencode.h \
    D:/app/qrcode/qrencode-master/qrencode/qrencode_inner.h \
    D:/app/qrcode/qrencode-master/qrencode/qrinput.h \
    D:/app/qrcode/qrencode-master/qrencode/qrspec.h \
    D:/app/qrcode/qrencode-master/qrencode/rscode.h \
    D:/app/qrcode/qrencode-master/qrencode/split.h \
    widget.h

FORMS += \
    widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    111.png

RESOURCES += \
    img.qrc
