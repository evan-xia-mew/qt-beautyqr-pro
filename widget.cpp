#include "widget.h"
#include "ui_widget.h"
#include "D:\app\qrcode\qrencode-master\qrencode\qrencode.h"
#include <QString>
#include <QDebug>
#include <QPainter>
#include <QBrush>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->groupQR->setStyleSheet("color:green");
    setWindowTitle("二维码生成器");
    //定义中心图片路径
    this->_me=QPixmap(":/new/111.png");
    on_pushButton_clicked();
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_clicked()
{
    //接受用户输入信息
    QString temp=ui->lineEdit->text();
    if(temp.isEmpty())
    {
        temp="嘿傻子！";
    }
    //转换字节数组
    //Qstring->Qbytearry
    _enter=temp.toUtf8();
    //打印测试，输出
    qDebug()<<temp<<_enter<<endl;
    //更新数据流
    update();
}

 void Widget::paintEvent(QPaintEvent *event)
 {
     //定义画家和画刷（黑色）
     QPainter paint(this);
     QBrush brush(Qt::black);
     paint.setBrush(brush);
     //函数调用：参数：需要转换成二维码的数据，二维码转换版本，二维码纠错能力，编码模式包含中文mode_8，是否大小写（小写转大写）
     QRcode* mqrcode=QRcode_encodeString(_enter.data(),8,QR_ECLEVEL_Q,QR_MODE_8,true);
     //接收完数据，转换二维码，1为黑格，0为白格
     if(mqrcode!=NULL)
     {
        //设置二维码大小
        this->_size=(this->width()-50)/mqrcode->width;
        this->_margin=(this->width()/2)-(mqrcode->width*_size)/2;
        unsigned char* evan=mqrcode->data;
        for(int x=0;x<mqrcode->width;x++)
        {
            for(int y=0;y<mqrcode->width;y++)
            {
                 if(*evan&1)
                 {
                     //groupQR向下平移30个像素点，-是向上平移
                     //如果真，画一个矩形
                     QRectF r(x*_size+_margin,y*_size+_margin+30,_size,_size);
                     paint.drawRect(r);
                 }
                 evan++;
             }
         }
         double scale=0.15;
         double icon_size=(this->width()-2.0*_margin)*scale;
         double ix=this->width()/2.0-icon_size/2.0;
         double iy=this->height()/2.0-icon_size/2.0;
         //中心小图片向下移动30像素单位
         QRect icon(ix,iy+30,icon_size,icon_size);
         paint.drawPixmap(icon,_me);
      }
 }
